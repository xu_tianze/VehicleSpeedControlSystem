////////////////////////////////////////////////////////////////////-=505-Tech=-//
//STM32F103C8T6学习板例程库-OLED
////////////////////////////////////////////////////////////////////-=505-Tech=-//

//***-=<头文件>=-***//---------------------------------------------//-=505-Tech=-//
#include "StudyBoard_OLED.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "sys.h"
#include "delay.h"
#include "StudyBoard_OLEDFont.h"
#include "StudyBoard_datatype.h"
#include "StudyBoard_system.h"

//***-=<对内接口定义>=-***//---------------------------------------//-=505-Tech=-//
/*OLED引脚定义*/
#define OLED_D0  PAout(5)
#define OLED_D1  PAout(4)
#define OLED_RES PCout(13)
#define OLED_DC  PBout(9)
/*OLED各项参数定义*/
#define XLevelL     0x00
#define XLevelH     0x10
#define Max_Column  128
#define Max_Row     64
#define	Brightness  0xFF
#define X_WIDTH     128
#define Y_WIDTH     64
/*指令类型定义*/
typedef enum {
  CmdType_Cmd  = 0,
  CmdType_Data = 1
} CmdType;

//***-=<内部函数实现>=-***//----------------------------------------//-=505-Tech=-//
/*------------------------------------------------------------------------------//
* 作用：通过SPI写入一个字节
* 说明：内部调用
* 参数：data  需要写入的内容
*       cmd  指令类型
* 返回：无
*///----------------------------------------------------------------------------//
static void __OLED_WriteByte(uint8_t data, CmdType cmd)
{
	uint8_t i;

	if(cmd)
	  OLED_DC = High;
	else
	  OLED_DC = Low;

	for(i=0;i<8;i++)
	{
		OLED_D0 = Low;

		if(data&0x80)
		   OLED_D1 = High;
		else
		   OLED_D1 = Low;

		OLED_D0 = High;
		data<<=1;
	}
	OLED_DC = High;
}
/*------------------------------------------------------------------------------//
* 作用：在屏幕上画点
* 说明：内部调用
* 参数：x   横坐标
*       y   纵坐标
* 返回：无
*///----------------------------------------------------------------------------//
static void __OLED_DrawPos(uint8_t x, uint8_t y)
{
	__OLED_WriteByte(0xb0+y,CmdType_Cmd);
	__OLED_WriteByte(((x&0xf0)>>4)|0x10,CmdType_Cmd);
	__OLED_WriteByte((x&0x0f)|0x01,CmdType_Cmd);
}

//***-=<对外接口函数实现>=-***//------------------------------------//-=505-Tech=-//
/*------------------------------------------------------------------------------//
* 作用：OLED初始化
* 说明：配置对应I/O口，并通过SPI写入初始化所需指令
* 参数：无
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_Init(void)
{
 	GPIO_InitTypeDef  GPIO_Struct;

 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOC, ENABLE);

	GPIO_StructInit(&GPIO_Struct);
 	GPIO_Struct.GPIO_Mode	= GPIO_Mode_Out_PP;
	GPIO_Struct.GPIO_Pin    = GPIO_Pin_4|GPIO_Pin_5;
 	GPIO_Init(GPIOA, &GPIO_Struct);
	GPIO_Struct.GPIO_Pin    = GPIO_Pin_9;
 	GPIO_Init(GPIOB, &GPIO_Struct);
	GPIO_Struct.GPIO_Pin    = GPIO_Pin_13;
 	GPIO_Init(GPIOC, &GPIO_Struct);
    OLED_D0  = Low;
    OLED_D1  = Low;
    OLED_RES = Low;
    OLED_DC  = Low;

    OLED_RES = High;
    delay_ms(100);
    OLED_RES = Low;
    delay_ms(100);
    OLED_RES = High;

	__OLED_WriteByte(0xAE,CmdType_Cmd);
	__OLED_WriteByte(0x00,CmdType_Cmd);
	__OLED_WriteByte(0x10,CmdType_Cmd);
	__OLED_WriteByte(0x40,CmdType_Cmd);
	__OLED_WriteByte(0x81,CmdType_Cmd);
	__OLED_WriteByte(0xCF,CmdType_Cmd);
	__OLED_WriteByte(0xA1,CmdType_Cmd);
	__OLED_WriteByte(0xC8,CmdType_Cmd);
	__OLED_WriteByte(0xA6,CmdType_Cmd);
	__OLED_WriteByte(0xA8,CmdType_Cmd);
	__OLED_WriteByte(0x3f,CmdType_Cmd);
	__OLED_WriteByte(0xD3,CmdType_Cmd);
	__OLED_WriteByte(0x00,CmdType_Cmd);
	__OLED_WriteByte(0xd5,CmdType_Cmd);
	__OLED_WriteByte(0x80,CmdType_Cmd);
	__OLED_WriteByte(0xD9,CmdType_Cmd);
	__OLED_WriteByte(0xF1,CmdType_Cmd);
	__OLED_WriteByte(0xDA,CmdType_Cmd);
	__OLED_WriteByte(0x12,CmdType_Cmd);
	__OLED_WriteByte(0xDB,CmdType_Cmd);
	__OLED_WriteByte(0x40,CmdType_Cmd);
	__OLED_WriteByte(0x20,CmdType_Cmd);
	__OLED_WriteByte(0x02,CmdType_Cmd);
	__OLED_WriteByte(0xA4,CmdType_Cmd);
	__OLED_WriteByte(0xA6,CmdType_Cmd);
	__OLED_WriteByte(0xAF,CmdType_Cmd);
  
	OLED_Clear();
  
  OLED_Display_On();
}
/*------------------------------------------------------------------------------//
* 作用：启用OLED显示
* 说明：向OLED写入使能显示指令
* 参数：无
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_Display_On(void)
{
	__OLED_WriteByte(0X8D,CmdType_Cmd);
	__OLED_WriteByte(0X14,CmdType_Cmd);
	__OLED_WriteByte(0XAF,CmdType_Cmd);
}
/*------------------------------------------------------------------------------//
* 作用：禁用OLED显示
* 说明：向OLED写入关闭显示指令
* 参数：无
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_Display_Off(void)
{
	__OLED_WriteByte(0X8D,CmdType_Cmd);
	__OLED_WriteByte(0X10,CmdType_Cmd);
	__OLED_WriteByte(0XAE,CmdType_Cmd);
}
/*------------------------------------------------------------------------------//
* 作用：加载显存
* 说明：将显存数组加载到OLED上,显存为128X8的数组
* 参数：GRAM[128][8] 显存数组
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_LoadGram(uint8_t GRAM[128][8])
{
  int i=0,j=0;
  
  for(i=0;i<8;i++)
  {
    __OLED_WriteByte(0xb0+i,CmdType_Cmd);
    __OLED_WriteByte(0x00,CmdType_Cmd);
    __OLED_WriteByte(0x10,CmdType_Cmd);
    
    for(j=0;j<128;j++)
    {
      __OLED_WriteByte(GRAM[j][i],CmdType_Data);
    }
  }	
}
/*------------------------------------------------------------------------------//
* 作用：清屏
* 说明：向OLED显存写入0
* 参数：无
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_Clear(void)
{
	uint8_t i,j;
	for(i=0;i<8;i++)
	{
		__OLED_WriteByte (0xb0+i,CmdType_Cmd);
		__OLED_WriteByte (0x02,CmdType_Cmd);
		__OLED_WriteByte (0x10,CmdType_Cmd);
		for(j=0;j<128;j++)
			__OLED_WriteByte(0,CmdType_Data);
	}
}
/*------------------------------------------------------------------------------//
* 作用：显示字符
* 说明：向OLED显存目标位置写入字库中的字符
* 参数：x     横坐标
*       y     纵坐标
*       chr   需要显示的字符
*       size  字体大小(填16或8)
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_ShowChar(uint8_t x, uint8_t y, char chr, uint8_t size)
{
	uint8_t i=0;
  
  #ifdef OLED_DEBUG
  if(x >= 128)  //横坐标需在0-127范围内
    System_Error("\nInvalid argument in\n\"OLED_ShowChar()\"\n\n0 < x < 127");
  if(y >= 8)    //纵坐标需在0-7范围内
    System_Error("\nInvalid argument in\n\"OLED_ShowChar()\"\n\n0 < y < 7");
  #endif

  chr-=' ';

  if(x>Max_Column-1)
  {
    x=0;
    y+=2;
  }

  if(size == 16)
  {
    __OLED_DrawPos(x,y);
    for(i=0;i<8;i++)
      __OLED_WriteByte(F8X16[chr*16+i],CmdType_Data);

    __OLED_DrawPos(x,y+1);
    for(i=0;i<8;i++)
    __OLED_WriteByte(F8X16[chr*16+i+8],CmdType_Data);
  }
  else
  {
    __OLED_DrawPos(x,y+1);
    for(i=0;i<6;i++)
    __OLED_WriteByte(F6x8[chr][i],CmdType_Data);
  }
}
/*------------------------------------------------------------------------------//
* 作用：在指定位置显示字符串
* 说明：向OLED显存目标位置写入字库中的字符
* 参数：x     横坐标
*       y     纵坐标
*       chr   需要显示的字符串地址
*       size  字体大小(填16或8)
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_ShowString(uint8_t x, uint8_t y, const char *chr, uint8_t size)
{
	uint8_t i;
  
  #ifdef OLED_DEBUG
  if(x >= 128)  //横坐标需在0-127范围内
    System_Error("\nInvalid argument in\n\"OLED_ShowString()\"\n\n0 < x < 127");
  if(y >= 8)    //纵坐标需在0-7范围内
    System_Error("\nInvalid argument in\n\"OLED_ShowString()\"\n\n0 < y < 7");
  #endif

  for(i=0;chr[i]!='\0';i++)
	{
    OLED_ShowChar(x,y,chr[i],size);

		x+=8;
		if(x>120)
    {
      x=0;
      y+=2;
    }
	}
}
/*------------------------------------------------------------------------------//
* 作用：全屏显示字符串
* 说明：从坐标(0,0)开始显示字符串，遇回车换行
* 参数：chr    需要显示的字符串地址
*       size   字体大小(填16或8)
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_ShowString_FullScreen(const char *chr, uint8_t size)
{
  uint8_t line,column,i,lineHeight,columnWidth;
  
  if(size == 16)
    lineHeight = 2,columnWidth=8;
  else
    lineHeight = 1,columnWidth=6;
    
  for(i=0,line=0;line<(8/lineHeight);line++)
  {
    for(column=0;column<(128/columnWidth);column++)
    {
      if(chr[i]=='\0')
        return;
      
      if(chr[i]=='\n')
      {
        i++;
        break;
      }
      
      OLED_ShowChar(column*columnWidth,line*lineHeight,chr[i++],size);
    }
  }
}
/*------------------------------------------------------------------------------//
* 作用：格式化输出字符串
* 说明：以printf的方式输出字符串
* 参数：size  字体大小(填16或8)
*      接下来的参数与printf相同
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_Printf(uint8_t size, const char *format, ...)
{
  uint8_t temp;
  char *s;
  va_list ap;
  
  temp = strlen(format);
  
  s = (char*)malloc(temp+1);
  
  va_start(ap,format);
  
	vsprintf(s, format, ap);
  
  OLED_ShowString_FullScreen(s,size);
  
  va_end(ap);
  
  free(s);
}
/*------------------------------------------------------------------------------//
* 作用：显示汉字
* 说明：显示字库"oledfont.h"中Chinese[][32]数组里对应序号的汉字
* 参数：x     横坐标
*       y     纵坐标
*       chr   需要显示的字符串指针
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_ShowChinese(uint8_t x, uint8_t y, uint8_t num)
{
	uint8_t i;
  
  #ifdef OLED_DEBUG
  if(x >= 128)  //横坐标需在0-127范围内
    System_Error("\nInvalid argument in\n\"OLED_ShowChinese()\"\n\n0 < x < 127");
  if(y >= 8)    //纵坐标需在0-7范围内
    System_Error("\nInvalid argument in\n\"OLED_ShowChinese()\"\n\n0 < y < 7");
  #endif

	__OLED_DrawPos(x,y);
	for(i=0;i<16;i++)
	{
		__OLED_WriteByte(Chinese[2*num][i],CmdType_Data);
	}

	__OLED_DrawPos(x,y+1);
	for(i=0;i<16;i++)
	{
		__OLED_WriteByte(Chinese[2*num+1][i],CmdType_Data);
	}
}
/*------------------------------------------------------------------------------//
* 作用：居中显示汉字串
* 说明：在指定行居中显示字库"oledfont.h"中Chinese[][32]数组里指定序号段的汉字
* 参数：line    显示的行数
*       start   显示汉字起始序号
*       end     显示汉字终止序号
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_ShowText(uint8_t line, uint8_t start, uint8_t end)
{
	uint8_t i;
  
  #ifdef OLED_DEBUG
  if(start > end)  //line需在0-7范围内
    System_Error("\nInvalid argument in\n\"OLED_ShowText()\"\n\n0 < line < 7");
  if(start > end)  //start需要在end前
    System_Error("\nInvalid argument in\n\"OLED_ShowText()\"\n\nstart <= end");
  #endif

	for(i=start;i<=end;i++)
		OLED_ShowChinese((i-start)*16+(128-(end-start+1)*16)/2,line,i);
}
/*------------------------------------------------------------------------------//
* 作用：清屏并显示LOGO
* 说明：清屏并显示在字库"oledfont.h"中事先取模的LOGO
* 参数：无
* 返回：无
*///----------------------------------------------------------------------------//
void OLED_ShowLogo(void)
{
	OLED_Clear();
	OLED_ShowText(3,0,5);
}

////////////////////////////////////////////////////////////////////-=505-Tech=-//
