////////////////////////////////////////////////////////////////////-=505-Tech=-//
//STM32F103C8T6学习板例程库-系统函数库
////////////////////////////////////////////////////////////////////-=505-Tech=-//

//***-=<头文件>=-***//---------------------------------------------//-=505-Tech=-//
#include "StudyBoard_system.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "stm32f10x.h"
#include "StudyBoard_datatype.h"
#include "StudyBoard_BEEP.h"
#include "StudyBoard_oled.h"

//***-=<内部函数实现>=-***//----------------------------------------//-=505-Tech=-//
/*------------------------------------------------------------------------------//
* 作用：禁用JTAG
* 说明：禁用JTAG调试可释放PA15、PB3、PB4，系统改用sw四线调试
* 参数：无
* 返回：无
*///----------------------------------------------------------------------------//
void System_JTAG_Disable(void)
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
  GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
}
/*------------------------------------------------------------------------------//
* 作用：报错
* 说明：关闭全局中断，程序进入死循环，oled以printf的方式显示错误信息,led与蜂鸣器打开
* 参数：与printf相同
* 返回：无
*///----------------------------------------------------------------------------//
#ifdef OLED_DEBUG
void System_Error(const char *errorMessage, ...)
{
  #define ErrorTitle "ERROR:\n"
  
  uint8_t temp[2];
  char *s;
  va_list ap;
  
  temp[0] = strlen(errorMessage);
  temp[1] = strlen(ErrorTitle);
  
  s = (char*)malloc(strlen(ErrorTitle)+temp[0]+1);
  strcpy(s,ErrorTitle);
  
  va_start(ap,errorMessage);
  
	vsprintf(s+temp[1], errorMessage, ap);
  
  OLED_ShowString_FullScreen(s,8);
  
  va_end(ap);
  
  free(s);
  
  __disable_irq();//关闭全局中断
  
  while(1)
  {
    BEEP_On();
  }
}
#endif //OLED_DEBUG

////////////////////////////////////////////////////////////////////-=505-Tech=-//
