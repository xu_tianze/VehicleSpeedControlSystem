////////////////////////////////////////////////////////////////////-=505-Tech=-//
//STM32F103C8T6学习板例程库-OLED
////////////////////////////////////////////////////////////////////-=505-Tech=-//
#ifndef __STUDYBOARD_OLED_H
#define __STUDYBOARD_OLED_H

//***-=<头文件>=-***//---------------------------------------------//-=505-Tech=-//
#include "STM32StudyBoard.h"

//***-=<接口定义>=-***//-------------------------------------------//-=505-Tech=-//
/*函数调用接口*/
void OLED_Init(void);
void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_LoadGram(uint8_t GRAM[128][8]);
void OLED_Clear(void);
void OLED_ShowChar(uint8_t x, uint8_t y, char chr, uint8_t size);
void OLED_ShowString(uint8_t x, uint8_t y, const char *chr, uint8_t size);
void OLED_ShowString_FullScreen(const char *chr, uint8_t size);
void OLED_Printf(uint8_t size, const char *format, ...);
void OLED_ShowChinese(uint8_t x, uint8_t y, uint8_t num);
void OLED_ShowText(uint8_t line, uint8_t start, uint8_t end);
void OLED_ShowLogo(void);

////////////////////////////////////////////////////////////////////-=505-Tech=-//
#endif //__STUDYBOARD_OLED_H
////////////////////////////////////////////////////////////////////-=505-Tech=-//
