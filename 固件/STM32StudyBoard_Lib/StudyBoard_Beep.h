////////////////////////////////////////////////////////////////////-=505-Tech=-//
//STM32F103C8T6学习板例程库-LED与蜂鸣器
////////////////////////////////////////////////////////////////////-=505-Tech=-//
#ifndef __STUDYBOARD_BEEP_H
#define __STUDYBOARD_BEEP_H

//***-=<头文件>=-***//---------------------------------------------//-=505-Tech=-//
#include "STM32StudyBoard.h"

//***-=<对外接口定义>=-***//---------------------------------------//-=505-Tech=-//
/*函数调用接口*/
void BEEP_Init(void);
void BEEP_On(void);
void BEEP_Off(void);
void BEEP_Inversion(void);
void BEEP_Hint(uint8_t flashTimes, uint16_t cycleTime, uint16_t onTime);
////////////////////////////////////////////////////////////////////-=505-Tech=-//
#endif //__STUDYBOARD_BEEP_H
////////////////////////////////////////////////////////////////////-=505-Tech=-//
