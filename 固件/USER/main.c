/**
 * @file main.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 主函数
 * @date 2021-04-19
 */

#include "usart.h"
#include "hc05.h"
#include "us100.h"
#include "jy60.h"
#include "STM32StudyBoard.h"
#include "key.h"
#include "motor.h"
#include "encoder.h"
#include "GUI.h"
#include "car.h"
#include "timer.h"
#include "ANO.h"

/**
 * @brief 主函数
 */
int main(void)
{
    delay_init(); // 该函数包含了SysTick_CLKSourceConfig()
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
    System_JTAG_Disable();

    uart1_init(9600, NULL);
    uart2_init(9600, us100_handler);
    uart3_init(9600, jy60_handler);
    hc05_init();
    BEEP_Init();
    OLED_Init();
    key_init();
    motor_init();
    encoder_init();
    TIM1_interrupt_init(car_handler);

    GUI_init();

    BEEP_Hint(2, 100, 50);

    while(true)
    {
        GUI_run();
        if(hc05_connected())
        {
            ANO_send_euler_angle(-jy60_get_roll(), -jy60_get_pitch(), jy60_get_yaw());
            ANO_main();
        }
    }
}
