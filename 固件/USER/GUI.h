/**
 * @file GUI.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief OLED用户界面
 * @date 2021-04-19
 */
#ifndef __GUI_H__
#define __GUI_H__

#include "SOLGUI_Include.h"

void GUI_init(void);
void GUI_run(void);

#endif // __GUI_H__
