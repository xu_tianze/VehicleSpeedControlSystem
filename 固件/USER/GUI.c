/**
 * @file GUI.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief OLED用户界面
 * @date 2021-04-19
 */

#include "GUI.h"
#include "pid.h"
#include "car.h"
#include "key.h"
#include "SOLGUI_Include.h"
#include "hc05.h"
#include "us100.h"
#include "jy60.h"

MENU_PAGE
    Homepage,
        Car,
            Car_VelocityPID,
                Car_VelocityPID_Left,
                Car_VelocityPID_Right,
            Car_AngularPID,
            Car_DistancePID,
        Modules,
            Modules_JY60,
            Modules_HC05,
            Modules_US100,
        Author;
extern PID VelocityLPID, VelocityRPID, AngularPID, DistancePID;

/**
 * @brief GUI初始化
 */
void GUI_init(void)
{
    SOLGUI_Init(&Homepage);
}

/**
 * @brief GUI运行
 */
void GUI_run(void)
{
    SOLGUI_InputKey(key_all_scan());
    SOLGUI_Menu_PageStage();
    SOLGUI_Refresh();
}

/**
 * @brief 主页
 */
__M_PAGE(Homepage, "Homepage", PAGE_NULL,
{
    SOLGUI_Cursor(6, 0, 3);

    SOLGUI_Widget_GotoPage  (0, &Car);
    SOLGUI_Widget_GotoPage  (1, &Modules);
    SOLGUI_Widget_GotoPage  (2, &Author);
});

/**
 * @brief 车辆信息页
 */
__M_PAGE(Car, "Car", &Homepage,
{
    static int16_t target = 0;

    SOLGUI_Cursor(6, 0, 8);

    SOLGUI_Widget_GotoPage  (0, &Car_VelocityPID);
    SOLGUI_Widget_GotoPage  (1, &Car_AngularPID);
    SOLGUI_Widget_GotoPage  (2, &Car_DistancePID);
    SOLGUI_Widget_Spin      (3, (const u8*)"Target:", INT16, 100, 0, &target);
    SOLGUI_Widget_OptionText(4, (const u8*)"VelocityL: %d", car_getVelocityL());
    SOLGUI_Widget_OptionText(5, (const u8*)"VelocityR: %d", car_getVelocityR());
    SOLGUI_Widget_OptionText(6, (const u8*)"Distance:  %f", car_getDistance());
    SOLGUI_Widget_OptionText(7, (const u8*)"Course:    %f", car_getCourse());

    car_setVelocity(target);

});

/**
 * @brief 速度PID调试页
 */
__M_PAGE(Car_VelocityPID, "VelocityPID", &Car,
{
    static int16_t target = 0;

    SOLGUI_Cursor(6, 0, 6);

    SOLGUI_Widget_Button    (0, (const u8*)"", car_runEnable_reverse);
    SOLGUI_Widget_OptionText(0, (const u8*)"Car Run Enable: %c", car_runEnable_status() ? 'O' : 'X');
    SOLGUI_Widget_Spin      (1, (const u8*)"Target:", INT16, 1000, 0, &target);
    SOLGUI_Widget_OptionText(2, (const u8*)"VelocityL:  %d", car_getVelocityL());
    SOLGUI_Widget_OptionText(3, (const u8*)"VelocityR:  %d", car_getVelocityR());
    SOLGUI_Widget_GotoPage  (4, &Car_VelocityPID_Left);
    SOLGUI_Widget_GotoPage  (5, &Car_VelocityPID_Right);

    car_setVelocityL(target);
    car_setVelocityR(target);
});

/**
 * @brief 左电机速度PID调试页
 */
__M_PAGE(Car_VelocityPID_Left, "VelocityPID Left", &Car_VelocityPID,
{
    static int16_t target = 0; 

    SOLGUI_Cursor(6, 0, 6);

    SOLGUI_Widget_Button    (0, (const u8*)"", car_runEnable_reverse);
    SOLGUI_Widget_OptionText(0, (const u8*)"Car Run Enable: %c", car_runEnable_status() ? 'O' : 'X');
    SOLGUI_Widget_OptionText(1, (const u8*)"VelocityL:  %d", car_getVelocityL());
    SOLGUI_Widget_Spin      (2, (const u8*)"Target:", INT16, 1000, 0, &target);
    SOLGUI_Widget_Spin      (3, (const u8*)"Kp:", FLT16, 10000, -10000, &VelocityLPID.kp);
    SOLGUI_Widget_Spin      (4, (const u8*)"Ki:", FLT16, 10000, -10000, &VelocityLPID.ki);
    SOLGUI_Widget_Spin      (5, (const u8*)"Kd:", FLT16, 10000, -10000, &VelocityLPID.kd);

    car_setVelocityL(target);
});

/**
 * @brief 右电机速度PID调试页
 */
__M_PAGE(Car_VelocityPID_Right, "VelocityRPID Right", &Car_VelocityPID,
{
    static int16_t target = 0; 

    SOLGUI_Cursor(6, 0, 6);

    SOLGUI_Widget_Button    (0, (const u8*)"", car_runEnable_reverse);
    SOLGUI_Widget_OptionText(0, (const u8*)"Car Run Enable: %c", car_runEnable_status() ? 'O' : 'X');
    SOLGUI_Widget_OptionText(1, (const u8*)"VelocityR:  %d", car_getVelocityR());
    SOLGUI_Widget_Spin      (2, (const u8*)"Target:", INT16, 1000, 0, &target);
    SOLGUI_Widget_Spin      (3, (const u8*)"Kp:", FLT16, 10000, -10000, &VelocityRPID.kp);
    SOLGUI_Widget_Spin      (4, (const u8*)"Ki:", FLT16, 10000, -10000, &VelocityRPID.ki);
    SOLGUI_Widget_Spin      (5, (const u8*)"Kd:", FLT16, 10000, -10000, &VelocityRPID.kd);

    car_setVelocityR(target);
});

/**
 * @brief 角度PID调试页
 */
__M_PAGE(Car_AngularPID, "AngularPID", &Car,
{
    static float target = 0; 

    SOLGUI_Cursor(6, 0, 8);

    SOLGUI_Widget_Button    (0, (const u8*)"", car_runEnable_reverse);
    SOLGUI_Widget_OptionText(0, (const u8*)"Car Run Enable: %c", car_runEnable_status() ? 'O' : 'X');
    SOLGUI_Widget_Button    (1, (const u8*)"", car_useAngularPID_reverse);
    SOLGUI_Widget_OptionText(1, (const u8*)"AngularPID En:  %c", car_useAngularPID_status() ? 'O' : 'X');
    SOLGUI_Widget_OptionText(2, (const u8*)"Course:     %f", car_getCourse());
    SOLGUI_Widget_Button    (3 , (const u8*)"Yaw Clear", jy60_yaw_clear);
    SOLGUI_Widget_Spin      (4, (const u8*)"Target:", FLT16, 360, -360, &target);
    SOLGUI_Widget_Spin      (5, (const u8*)"Kp:", FLT16, 10000, -10000, &AngularPID.kp);
    SOLGUI_Widget_Spin      (6, (const u8*)"Ki:", FLT16, 10000, -10000, &AngularPID.ki);
    SOLGUI_Widget_Spin      (7, (const u8*)"Kd:", FLT16, 10000, -10000, &AngularPID.kd);

    car_setCourse(target);
});

/**
 * @brief 距离PID调试页
 */
__M_PAGE(Car_DistancePID, "DistancePID", &Car,
{
    static float target = 0.3; 

    SOLGUI_Cursor(6, 0, 9);

    SOLGUI_Widget_Button    (0, (const u8*)"", car_runEnable_reverse);
    SOLGUI_Widget_OptionText(0, (const u8*)"Car Run Enable: %c", car_runEnable_status() ? 'O' : 'X');
    SOLGUI_Widget_Button    (1, (const u8*)"", car_useAngularPID_reverse);
    SOLGUI_Widget_OptionText(1, (const u8*)"AngularPID En:  %c", car_useAngularPID_status() ? 'O' : 'X');
    SOLGUI_Widget_Button    (2, (const u8*)"", car_useDistancePID_reverse);
    SOLGUI_Widget_OptionText(2, (const u8*)"DistancePID En: %c", car_useDistancePID_status() ? 'O' : 'X');
    SOLGUI_Widget_OptionText(3, (const u8*)"Distance:   %f", car_getDistance());
    SOLGUI_Widget_Button    (4 , (const u8*)"Yaw Clear", jy60_yaw_clear);
    SOLGUI_Widget_Spin      (5, (const u8*)"Target:", FLT16, 10, 0, &target);
    SOLGUI_Widget_Spin      (6, (const u8*)"Kp:", FLT16, 10000, -10000, &DistancePID.kp);
    SOLGUI_Widget_Spin      (7, (const u8*)"Ki:", FLT16, 10000, -10000, &DistancePID.ki);
    SOLGUI_Widget_Spin      (8, (const u8*)"Kd:", FLT16, 10000, -10000, &DistancePID.kd);

    car_setDistance(target);
});

/**
 * @brief 模块信息页
 */
__M_PAGE(Modules, "Modules", &Homepage,
{
    SOLGUI_Cursor(6, 0, 3);

    SOLGUI_Widget_GotoPage  (0, &Modules_JY60);
    SOLGUI_Widget_GotoPage  (1, &Modules_HC05);
    SOLGUI_Widget_GotoPage  (2, &Modules_US100);
});

/**
 * @brief jy60模块相关操作
 */
__M_PAGE(Modules_JY60, "JY60", &Modules,
{
    SOLGUI_Cursor(6, 0, 15);

    SOLGUI_Widget_Button    (0 , (const u8*)"Yaw Clear",      jy60_yaw_clear);
    SOLGUI_Widget_Button    (1 , (const u8*)"Acc Correct",    jy60_acc_correct);
    SOLGUI_Widget_OptionText(2 , (const u8*)"Roll:  %.3f",    jy60_get_roll_branched());
    SOLGUI_Widget_OptionText(3 , (const u8*)"Pitch: %.3f",    jy60_get_pitch_branched());
    SOLGUI_Widget_OptionText(4 , (const u8*)"Yaw:   %.3f",    jy60_get_yaw_branched());
    SOLGUI_Widget_OptionText(5 , (const u8*)"Temp:  %.3f",    jy60_get_temperature());
    SOLGUI_Widget_Button    (6 , (const u8*)"Sleep",          jy60_sleep);
    SOLGUI_Widget_Button    (7 , (const u8*)"Set Level",      jy60_set_level);
    SOLGUI_Widget_Button    (8 , (const u8*)"Set Vertical",   jy60_set_vertical);
    SOLGUI_Widget_OptionText(9 , (const u8*)"Ax:    %.3f",    jy60_get_ax());
    SOLGUI_Widget_OptionText(10, (const u8*)"Ay:    %.3f",    jy60_get_ay());
    SOLGUI_Widget_OptionText(11, (const u8*)"Az:    %.3f",    jy60_get_az());
    SOLGUI_Widget_OptionText(12, (const u8*)"Wx:    %.3f",    jy60_get_wx());
    SOLGUI_Widget_OptionText(13, (const u8*)"Wy:    %.3f",    jy60_get_wy());
    SOLGUI_Widget_OptionText(14, (const u8*)"Wz:    %.3f",    jy60_get_wz());
});

/**
 * @brief HC05模块相关操作
 */
__M_PAGE(Modules_HC05, "HC05", &Modules,
{
    SOLGUI_Cursor(6, 0, 3);

    SOLGUI_Widget_OptionText(0, (const u8*)"HC05 state: %c", hc05_connected() ? 'O' : 'X');
    SOLGUI_Widget_Button    (1, (const u8*)"ON",             hc05_on);
    SOLGUI_Widget_Button    (2, (const u8*)"OFF",            hc05_off);
});

/**
 * @brief US100模块相关操作
 */
__M_PAGE(Modules_US100, "US100", &Modules,
{
    SOLGUI_Cursor(6, 0, 1);

    SOLGUI_Widget_OptionText(0, (const u8*)"Distance: %f", us100_measure());
});

/**
 * @brief 作者信息页
 */
__M_PAGE(Author, "Author", &Homepage,
{
    SOLGUI_Widget_OptionText(0, (const u8*)"Name:     Ning Ning");
    SOLGUI_Widget_OptionText(1, (const u8*)"ID:       171543518");
});

/*===========================新建菜单页面格式及常用函数=============================
__M_PAGE(name(),pageTitle,parentPage,
//（该页面名称，“页面标题”，&上级页面名）
{

    SOLGUI_Cursor( uint8_t rowBorder_Top, uint8_t rowBorder_Bottom, uint8_t option_num)
    //（上边界行，下边界行，选项个数）

    页面跳转控件:
    SOLGUI_Widget_GotoPage( uint8_t USN,MENU_PAGE *page)
    //(该选项所处位置,	&页面地址)

    按键控件：
    SOLGUI_Widget_Button( uint8_t USN,const  uint8_t *name,void (*func)(void))
    //(该选项所处位置,"该选项显示的文字",&按下该按键时执行的无参数void类型函数);

    数字旋钮控件：
    SOLGUI_Widget_Spin( uint8_t USN,const  uint8_t *name, uint8_t type,double max,double min,void* value)
    //(该选项所处位置,"该选项显示的文字",变量类型（如INT32）,变化上限,变化下限,&变量地址);

    自由文本控件：
    SOLGUI_Widget_Text( uint32_t x0, uint32_t y0, uint8_t mode,const  uint8_t* str,...)
    //(横坐标,纵坐标,字号（如F4X6）,"需要显示的文字（格式同printf）",用法同printf字符串后的参数);

});
================================================================================*/
