/**
 * @file app.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 用来由GUI调用的用户应用
 * @date 2021-04-19
 */

#ifndef __APP_H__
#define __APP_H__

#include "stm32f10x.h"
#include <stdint.h>

#endif //__APP_H__
