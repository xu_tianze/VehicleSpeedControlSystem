/**
 * @file usart.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 串口驱动
 * @date 2021-04-22
 */

#include "usart.h"
#include <stdio.h>
#include <stm32f10x.h>
#include <stdint.h>

/* 函数指针定义 */
static void (*usart1_handler)(uint8_t res);
static void (*usart2_handler)(uint8_t res[]);
static void (*usart3_handler)(uint8_t res);

static uint8_t USART2_Buff[2];

/**
 * @brief MicroLIB库的文件输出函数重定义，用于支持标准库printf函数
 * @param ch 输出的字符
 * @param f 目标文件指针
 * @return 输出字符个数
 */
int fputc(int ch, FILE *f)
{
	USART_SendData(USART1, (uint8_t)ch);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET){}

    return ch;
}

/**
 * @brief 串口1初始化
 * @param bound 波特率
 * @param handler 中断处理句柄
 */
void uart1_init(uint32_t bound, void(*handler)(uint8_t res))
{
    GPIO_InitTypeDef GPIO_Struct;
    USART_InitTypeDef USART_Struct;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_StructInit(&GPIO_Struct);
    GPIO_Struct.GPIO_Mode   = GPIO_Mode_AF_PP;
    GPIO_Struct.GPIO_Pin    = GPIO_Pin_9;
    GPIO_Init(GPIOA, &GPIO_Struct);
    GPIO_Struct.GPIO_Mode   = GPIO_Mode_IN_FLOATING;
    GPIO_Struct.GPIO_Pin    = GPIO_Pin_10;
    GPIO_Init(GPIOA, &GPIO_Struct);

    USART_StructInit(&USART_Struct);
    USART_Struct.USART_BaudRate             = bound;
    USART_Struct.USART_WordLength           = USART_WordLength_8b;
    USART_Struct.USART_StopBits             = USART_StopBits_1;
    USART_Struct.USART_Parity               = USART_Parity_No;
    USART_Struct.USART_HardwareFlowControl  = USART_HardwareFlowControl_None;
    USART_Struct.USART_Mode                 = USART_Mode_Tx | USART_Mode_Rx;
    USART_Init(USART1, &USART_Struct);

    if(handler)
    {
        NVIC_InitTypeDef NVIC_InitStructure;

        usart1_handler = handler;

        NVIC_InitStructure.NVIC_IRQChannel                      = USART1_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority    = 3;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority           = 3;
        NVIC_InitStructure.NVIC_IRQChannelCmd                   = ENABLE;
        NVIC_Init(&NVIC_InitStructure);

        USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
    }
    
    USART_Cmd(USART1, ENABLE);
}

/**
 * @brief 串口2初始化
 * @param bound 波特率
 * @param handler 中断处理句柄
 */
void uart2_init(uint32_t bound, void(*handler)(uint8_t res[]))
{
    GPIO_InitTypeDef GPIO_Struct;
    USART_InitTypeDef USART_Struct;
    DMA_InitTypeDef DMA_Struct;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

    GPIO_StructInit(&GPIO_Struct);
    GPIO_Struct.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Struct.GPIO_Pin  = GPIO_Pin_2;
    GPIO_Init(GPIOA, &GPIO_Struct);
    GPIO_Struct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Struct.GPIO_Pin  = GPIO_Pin_3;
    GPIO_Init(GPIOA, &GPIO_Struct);

    USART_StructInit(&USART_Struct);
    USART_Struct.USART_BaudRate             = bound;
    USART_Struct.USART_WordLength           = USART_WordLength_8b;
    USART_Struct.USART_StopBits             = USART_StopBits_1;
    USART_Struct.USART_Parity               = USART_Parity_No;
    USART_Struct.USART_HardwareFlowControl  = USART_HardwareFlowControl_None;
    USART_Struct.USART_Mode                 = USART_Mode_Tx | USART_Mode_Rx;
    USART_Init(USART2, &USART_Struct);

    if(handler)
    {
        NVIC_InitTypeDef NVIC_InitStructure;

        usart2_handler = handler;

        NVIC_InitStructure.NVIC_IRQChannel                      = USART2_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority    = 3;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority           = 2;
        NVIC_InitStructure.NVIC_IRQChannelCmd                   = ENABLE;
        NVIC_Init(&NVIC_InitStructure);

        USART_ITConfig(USART2, USART_IT_IDLE, ENABLE);
    }

    USART_Cmd(USART2, ENABLE);

    DMA_StructInit(&DMA_Struct);
    DMA_Struct.DMA_PeripheralBaseAddr = (uint32_t)(&(USART2->DR));
    DMA_Struct.DMA_MemoryBaseAddr     = (uint32_t)USART2_Buff;
    DMA_Struct.DMA_BufferSize         = 2;
    DMA_Struct.DMA_MemoryInc          = DMA_MemoryInc_Enable;
    DMA_Struct.DMA_Priority           = DMA_Priority_Medium;
    DMA_Init(DMA1_Channel6, &DMA_Struct);
		
    DMA_ClearFlag(DMA1_FLAG_GL6);
    DMA_Cmd(DMA1_Channel6, ENABLE);
		
    USART_DMACmd(USART2, USART_DMAReq_Rx, ENABLE);
}

/**
 * @brief 串口3初始化
 * @param bound 波特率
 * @param handler 中断处理句柄
 */
void uart3_init(uint32_t bound, void(*handler)(uint8_t res))
{
    GPIO_InitTypeDef GPIO_Struct;
    USART_InitTypeDef USART_Struct;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);

    GPIO_StructInit(&GPIO_Struct);
    GPIO_Struct.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Struct.GPIO_Pin  = GPIO_Pin_10;
    GPIO_Init(GPIOB, &GPIO_Struct);
    GPIO_Struct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Struct.GPIO_Pin  = GPIO_Pin_11;
    GPIO_Init(GPIOB, &GPIO_Struct);

    USART_StructInit(&USART_Struct);
    USART_Struct.USART_BaudRate             = bound;
    USART_Struct.USART_WordLength           = USART_WordLength_8b;
    USART_Struct.USART_StopBits             = USART_StopBits_1;
    USART_Struct.USART_Parity               = USART_Parity_No;
    USART_Struct.USART_HardwareFlowControl  = USART_HardwareFlowControl_None;
    USART_Struct.USART_Mode                 = USART_Mode_Tx | USART_Mode_Rx;
	USART_Init(USART3, &USART_Struct);

    if(handler)
    {
        NVIC_InitTypeDef NVIC_InitStructure;

        usart3_handler = handler;

        NVIC_InitStructure.NVIC_IRQChannel                      = USART3_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority    = 3;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority           = 1;
        NVIC_InitStructure.NVIC_IRQChannelCmd                   = ENABLE;
        NVIC_Init(&NVIC_InitStructure);

	    USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
    }
    
	USART_Cmd(USART3, ENABLE);
}

/**
 * @brief 串口1标准库中断服务函数重定义
 */
void USART1_IRQHandler(void)
{
    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {
        usart1_handler(USART_ReceiveData(USART1));
    }
}

/**
 * @brief 串口2标准库中断服务函数重定义
 */
void USART2_IRQHandler(void)
{
    if(USART_GetITStatus(USART2, USART_IT_IDLE) != RESET)
    {
        USART2->SR;
        USART2->DR;
        DMA_Cmd(DMA1_Channel6, DISABLE);
        
        usart2_handler(USART2_Buff);

        DMA_SetCurrDataCounter(DMA1_Channel6, 2);
        DMA_ClearFlag(DMA1_FLAG_GL6);
        DMA_ClearFlag(DMA1_FLAG_TC6);
        DMA_ClearFlag(DMA1_FLAG_TE6);
        DMA_Cmd(DMA1_Channel6, ENABLE);
    }
}

/**
 * @brief 串口3标准库中断服务函数重定义
 */
void USART3_IRQHandler(void)
{
    if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
    {
        usart3_handler(USART_ReceiveData(USART3));
    }
}
