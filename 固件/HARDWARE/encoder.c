/**
 * @file encoder.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 电机编码器驱动
 * @date 2021-04-22
 */

#include "encoder.h"
#include <stm32f10x.h>

/**
 * @brief 编码器初始化
 */
void encoder_init(void)
{
    GPIO_InitTypeDef GPIO_Struct;
    TIM_TimeBaseInitTypeDef	TIM_TimeBaseStruct;
    TIM_ICInitTypeDef TIM_ICStruct;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2 | RCC_APB1Periph_TIM3, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_StructInit(&GPIO_Struct);
    GPIO_Struct.GPIO_Mode   = 	GPIO_Mode_IN_FLOATING;
    GPIO_Struct.GPIO_Pin    =	GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_Init(GPIOA, &GPIO_Struct);

    TIM_TimeBaseStructInit(&TIM_TimeBaseStruct);
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStruct);
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStruct);

    TIM_EncoderInterfaceConfig(TIM2, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
    TIM_EncoderInterfaceConfig(TIM3, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);

    TIM_ICStructInit(&TIM_ICStruct);
    TIM_ICStruct.TIM_ICFilter = 10;
    TIM_ICInit(TIM2, &TIM_ICStruct);
    TIM_ICInit(TIM3, &TIM_ICStruct);

    TIM_SetCounter(TIM2, 0);
    TIM_SetCounter(TIM3, 0);

    TIM_Cmd(TIM2, ENABLE);
    TIM_Cmd(TIM3, ENABLE);
}

/**
 * @brief 读取左编码器测得的速度值
 * @return 测得的速度值
 */
int16_t encoder_get_speedL(void)
{
    int16_t ret = -TIM_GetCounter(TIM3);
    TIM_SetCounter(TIM3, 0);
    return ret;
}

/**
 * @brief 读取右编码器测得的速度值
 * @return 测得的速度值
 */
int16_t encoder_get_speedR(void)
{
    int16_t ret = TIM_GetCounter(TIM2);
    TIM_SetCounter(TIM2, 0);
    return ret;
}
