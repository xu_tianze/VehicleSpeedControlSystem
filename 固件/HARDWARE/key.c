/**
 * @file key.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 机械按键驱动
 * @date 2021-04-19
 */

#include "key.h"
#include <stm32f10x.h>
#include <stdint.h>
#include <stdbool.h>
#include "delay.h"

/**
 * @brief 按键及对应LED初始化
 */
void key_init(void)
{
    GPIO_InitTypeDef GPIO_Struct;

    RCC_APB2PeriphClockCmd(KEY_LEFT_CLK | KEY_LEFT_LED_CLK | KEY_OK_CLK | KEY_OK_LED_CLK | KEY_RIGHT_CLK | KEY_RIGHT_LED_CLK, ENABLE);

    GPIO_StructInit(&GPIO_Struct);
    GPIO_Struct.GPIO_Mode	= KEY_PRESS_LEVEL ? GPIO_Mode_IPD : GPIO_Mode_IPU;
    GPIO_Struct.GPIO_Pin    = KEY_LEFT_PIN;
    GPIO_Init(KEY_LEFT_GPIO,  &GPIO_Struct);
    GPIO_Struct.GPIO_Pin    = KEY_OK_PIN;
    GPIO_Init(KEY_OK_GPIO,    &GPIO_Struct);
    GPIO_Struct.GPIO_Pin    = KEY_RIGHT_PIN;
    GPIO_Init(KEY_RIGHT_GPIO, &GPIO_Struct);
    GPIO_Struct.GPIO_Mode	= GPIO_Mode_Out_PP;
    GPIO_Struct.GPIO_Pin    = KEY_LEFT_LED_PIN;
    GPIO_Init(KEY_LEFT_LED_GPIO,  &GPIO_Struct);
    GPIO_Struct.GPIO_Pin    = KEY_OK_LED_PIN;
    GPIO_Init(KEY_OK_LED_GPIO,    &GPIO_Struct);
    GPIO_Struct.GPIO_Pin    = KEY_RIGHT_LED_PIN;
    GPIO_Init(KEY_RIGHT_LED_GPIO, &GPIO_Struct);

    key_all_led_off();
}

/**
 * @brief 打开led
 */
void key_left_led_on(void)
{
    if(KEY_LED_ON_LEVEL == true)
    {
        GPIO_SetBits(KEY_LEFT_LED_GPIO, KEY_LEFT_LED_PIN);
    }
    else
    {
        GPIO_ResetBits(KEY_LEFT_LED_GPIO, KEY_LEFT_LED_PIN);
    }
}
void key_ok_led_on(void)
{
    if(KEY_LED_ON_LEVEL == true)
    {
        GPIO_SetBits(KEY_OK_LED_GPIO, KEY_OK_LED_PIN);
    }
    else
    {
        GPIO_ResetBits(KEY_OK_LED_GPIO, KEY_OK_LED_PIN);
    }
}
void key_right_led_on(void)
{
    if(KEY_LED_ON_LEVEL == true)
    {
        GPIO_SetBits(KEY_RIGHT_LED_GPIO, KEY_RIGHT_LED_PIN);
    }
    else
    {
        GPIO_ResetBits(KEY_RIGHT_LED_GPIO, KEY_RIGHT_LED_PIN);
    }
}
void key_all_led_on(void)
{
    key_left_led_on();
    key_ok_led_on();
    key_right_led_on();
}

/**
 * @brief 关闭led
 */
void key_left_led_off(void)
{
    if(KEY_LED_ON_LEVEL == true)
    {
        GPIO_ResetBits(KEY_LEFT_LED_GPIO, KEY_LEFT_LED_PIN);
    }
    else
    {
        GPIO_SetBits(KEY_LEFT_LED_GPIO, KEY_LEFT_LED_PIN);
    }
}
void key_ok_led_off(void)
{
    if(KEY_LED_ON_LEVEL == true)
    {
        GPIO_ResetBits(KEY_OK_LED_GPIO, KEY_OK_LED_PIN);
    }
    else
    {
        GPIO_SetBits(KEY_OK_LED_GPIO, KEY_OK_LED_PIN);
    }
}
void key_right_led_off(void)
{
    if(KEY_LED_ON_LEVEL == true)
    {
        GPIO_ResetBits(KEY_RIGHT_LED_GPIO, KEY_RIGHT_LED_PIN);
    }
    else
    {
        GPIO_SetBits(KEY_RIGHT_LED_GPIO, KEY_RIGHT_LED_PIN);
		}
}
void key_all_led_off(void)
{
    key_left_led_off();
    key_ok_led_off();
    key_right_led_off();
}

/**
 * @brief led闪烁两次
 */
void key_all_led_blink(void)
{
    key_all_led_off();
    delay_ms(50);
    key_all_led_on();
    delay_ms(50);
    key_all_led_off();
    delay_ms(50);
    key_all_led_on();
    delay_ms(50);
    key_all_led_off();
}

/**
 * @brief 按键状态扫描
 * @return true  按键被按下
 * @return false 按键未按下
 */
bool key_left_status(void)
{
    if(GPIO_ReadInputDataBit(KEY_LEFT_GPIO, KEY_LEFT_PIN) == KEY_PRESS_LEVEL)
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool key_ok_status(void)
{
    if(GPIO_ReadInputDataBit(KEY_OK_GPIO, KEY_OK_PIN) == KEY_PRESS_LEVEL)
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool key_right_status(void)
{
    if(GPIO_ReadInputDataBit(KEY_RIGHT_GPIO, KEY_RIGHT_PIN) == KEY_PRESS_LEVEL)
    {
        return true;
    }
    else
    {
        return false;
    }
}
bool key_all_status(void)
{
    if(key_left_status() || key_ok_status() || key_right_status())
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * @brief 按键扫描
 * @return 0 按键未按下
 * @return 1 按键被短按
 * @return 2 按键被长按
 */
uint8_t key_left_scan(void)
{
    if(key_left_status())
    {
        delay_ms(KEY_SCAN_FLITER_TIME);
        if(key_left_status())
        {
            key_left_led_on();
            for(int i = 0; i < KEY_SCAN_LONG_PRESS / 50; i++)
            {
                if(!key_left_status())
                {
                    delay_ms(KEY_SCAN_FLITER_TIME);
                    if(!key_left_status())
                    {
                        key_left_led_off();
                        return 1;
                    }
                }
                else
                {
                    delay_ms(KEY_SCAN_FLITER_TIME);
                }
                delay_ms(50 - KEY_SCAN_FLITER_TIME);
            }
            
            key_all_led_blink();

            while(key_left_status());
            delay_ms(KEY_SCAN_FLITER_TIME);
            while(key_left_status());
            return 2;
        }
    }
    return 0;
}
uint8_t key_ok_scan(void)
{
    if(key_ok_status())
    {
        delay_ms(KEY_SCAN_FLITER_TIME);
        if(key_ok_status())
        {
            key_ok_led_on();
            for(int i = 0; i < KEY_SCAN_LONG_PRESS / 50; i++)
            {
                if(!key_ok_status())
                {
                    delay_ms(KEY_SCAN_FLITER_TIME);
                    if(!key_ok_status())
                    {
                        key_ok_led_off();
                        return 1;
                    }
                }
                else
                {
                    delay_ms(KEY_SCAN_FLITER_TIME);
                }
                delay_ms(50 - KEY_SCAN_FLITER_TIME);
            }
            
            key_all_led_blink();

            while(key_ok_status());
            delay_ms(KEY_SCAN_FLITER_TIME);
            while(key_ok_status());
            return 2;
        }
    }
    return 0;
}
uint8_t key_right_scan(void)
{
    if(key_right_status())
    {
        delay_ms(KEY_SCAN_FLITER_TIME);
        if(key_right_status())
        {
            key_right_led_on();
            for(int i = 0; i < KEY_SCAN_LONG_PRESS / 50; i++)
            {
                if(!key_right_status())
                {
                    delay_ms(KEY_SCAN_FLITER_TIME);
                    if(!key_right_status())
                    {
                        key_right_led_off();
                        return 1;
                    }
                }
                else
                {
                    delay_ms(KEY_SCAN_FLITER_TIME);
                }
                delay_ms(50 - KEY_SCAN_FLITER_TIME);
            }
            
            key_all_led_blink();

            while(key_right_status());
            delay_ms(KEY_SCAN_FLITER_TIME);
            while(key_right_status());
            return 2;
        }
    }
    return 0;
}

/**
 * @brief 扫描所有按键
 * @return uint8_t 
 */
uint8_t key_all_scan(void)
{
    uint8_t ret;

    if((ret = key_left_scan()) != false)
    {
        if(ret == 1)
        {
            return KEY_LEFT_PRESS_SHORT;
        }
        else 
        {
            return KEY_LEFT_PRESS_LONG;
        }
    }
    else if((ret = key_ok_scan()) != false)
    {
        if(ret == 1)
        {
            return KEY_OK_PRESS_SHORT;
        }
        else
        {
            return KEY_OK_PRESS_LONG;
        }
    }
    else if((ret = key_right_scan()) != false)
    {
        if(ret == 1)
        {
            return KEY_RIGHT_PRESS_SHORT;
        }
        else
        {
            return KEY_RIGHT_PRESS_LONG;
        }
    }
    return 0;
}
