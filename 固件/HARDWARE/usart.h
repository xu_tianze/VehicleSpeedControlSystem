/**
 * @file usart.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 串口驱动
 * @date 2021-04-22
 */

#ifndef __USART_H__
#define __USART_H__

#include <stdint.h>

void uart1_init(uint32_t bound, void(*handler)(uint8_t res));
void uart2_init(uint32_t bound, void(*handler)(uint8_t res[]));
void uart3_init(uint32_t bound, void(*handler)(uint8_t res));

#endif // __USART_H__
