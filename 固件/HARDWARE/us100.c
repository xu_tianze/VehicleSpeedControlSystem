/**
 * @file us100.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief US-100串口超声波模块驱动
 * @date 2021-04-22
 */

#include "us100.h"
#include <stm32f10x.h>
#include <stdint.h>
#include <stdbool.h>

#define USARTx USART2

static float distance = 0;
volatile bool measureFinish = true;

/**
 * @brief 串口发送一个字节
 * @param data 需要发送的一字节数据
 */
static void usart_send_byte(uint8_t data)
{
    if(USARTx->SR & 0X40)
    {
        USARTx->DR = data;
    }
    // USARTx->DR = data;
    // while(!(USARTx->SR & 0X40));
}

/**
 * @brief 串口数据处理句柄，应放于串口中断服务函数中
 * @param res 串口读到的数据
 */
void us100_handler(uint8_t res[])
{
    measureFinish = true;
    distance = ((res[0] << 8) + res[1]) / 1000.0;
}

/**
 * @brief 获得测量值
 * @return 测得的距离
 */
float us100_measure(void)
{
    if(measureFinish)
    {
        measureFinish = false;
        usart_send_byte(0x55);
    }
    return distance;
}
