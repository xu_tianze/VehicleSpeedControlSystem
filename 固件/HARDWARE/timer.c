/**
 * @file timer.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 定时器配置
 * @date 2021-04-19
 */

#include "timer.h"

static void (*TIM1_interrupt_handler)();

/**
 * @brief 定时器中断初始化
 * @param handler 中断处理句柄
 * @note 频率=72000000/(Period+1)/(Prescaler+1)
 */
void TIM1_interrupt_init(void(*handler)())
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    TIM1_interrupt_handler = handler;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

    TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure);
    TIM_TimeBaseInitStructure.TIM_Prescaler     = 7200 - 1;
    TIM_TimeBaseInitStructure.TIM_Period        = 100 - 1;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode   = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseInitStructure);

    TIM_ITConfig(TIM1, TIM_IT_Update, ENABLE);

    NVIC_InitStructure.NVIC_IRQChannel                   = TIM1_UP_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority        = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd                = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    TIM_Cmd(TIM1, ENABLE);
}

/**
 * @brief 定时器1中断服务函数
 */
void TIM1_UP_IRQHandler(void)
{
    TIM_ClearITPendingBit(TIM1, TIM_IT_Update); // 清除中断标志位

    TIM1_interrupt_handler(); // 执行中断处理句柄
}
