/**
 * @file jy60.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief jy60串口陀螺仪模块驱动
 * @date 2021-04-22
 */

#ifndef __JY60_H__
#define __JY60_H__

#include <stdint.h>

void jy60_handler(uint8_t res);
float jy60_get_ax(void);
float jy60_get_ay(void);
float jy60_get_az(void);
float jy60_get_wx(void);
float jy60_get_wy(void);
float jy60_get_wz(void);
float jy60_get_roll(void);
float jy60_get_pitch(void);
float jy60_get_yaw(void);
float jy60_get_roll_branched(void);
float jy60_get_pitch_branched(void);
float jy60_get_yaw_branched(void);
float jy60_get_temperature(void);
void jy60_yaw_clear(void);
void jy60_acc_correct(void);
void jy60_sleep(void);
void jy60_set_level(void);
void jy60_set_vertical(void);

#endif // __JY60_H__
