/**
 * @file motor.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 电机驱动
 * @date 2021-04-19
 */

#include "motor.h"
#include <stdbool.h>

/**
 * @brief 电机初始化
 */
void motor_init(void)
{  
    GPIO_InitTypeDef GPIO_Struct;
    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE);

    GPIO_StructInit(&GPIO_Struct);
    GPIO_Struct.GPIO_Mode  = GPIO_Mode_Out_PP;
    GPIO_Struct.GPIO_Pin   = GPIO_Pin_12 | GPIO_Pin_15;
    GPIO_Init(GPIOA, &GPIO_Struct);
    GPIO_ResetBits(GPIOA, GPIO_Pin_12 | GPIO_Pin_15);
    GPIO_Struct.GPIO_Pin   = GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_4;
    GPIO_Init(GPIOB, &GPIO_Struct);
    GPIO_ResetBits(GPIOB, GPIO_Pin_3 | GPIO_Pin_4);
    GPIO_Struct.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_Struct.GPIO_Pin   = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_Init(GPIOB, &GPIO_Struct);

    TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure);
    TIM_TimeBaseInitStructure.TIM_Period    = 1000 - 1;
    TIM_TimeBaseInitStructure.TIM_Prescaler = 72 - 1;
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseInitStructure);

    TIM_OCStructInit(&TIM_OCInitStructure);
    TIM_OCInitStructure.TIM_OCMode      = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OC1Init(TIM4, &TIM_OCInitStructure);
    TIM_OC2Init(TIM4, &TIM_OCInitStructure);

    TIM_OC1PreloadConfig(TIM4, TIM_OCPreload_Enable);
    TIM_OC2PreloadConfig(TIM4, TIM_OCPreload_Enable);

    TIM_ARRPreloadConfig(TIM4, ENABLE);
    TIM_CtrlPWMOutputs(TIM4,ENABLE);

    TIM_SetCompare1(TIM4, 0);
    TIM_SetCompare2(TIM4, 0);

    motor_off();
    TIM_Cmd(TIM4, ENABLE);
}

/**
 * @brief 打开电机
 */
void motor_on(void)
{
    MOTOR_EN = true;
}

/**
 * @brief 关闭电机
 */
void motor_off(void)
{
    MOTOR_EN = false;
}

/**
 * @brief 设置电机PWM
 * @param pwmL 左电机PWM，-1000 ~ 1000
 * @param pwmR 右电机PWM，-1000 ~ 1000
 */
void motor_pwm_set(int pwmL, int pwmR)
{
    if(pwmL >= 1000)
    {
        pwmL = 1000;
    }
    else if(pwmL <= -1000)
    {
        pwmL = -1000;
    }

    if(pwmL > 0)
    {
        MOTORL_IN1 = true;
        MOTORL_IN2 = false;
        TIM_SetCompare2(TIM4, pwmL);
    }
    else if(pwmL < 0)
    {
        MOTORL_IN1 = false;
        MOTORL_IN2 = true;
        TIM_SetCompare2(TIM4, -pwmL);
    }
    else
    {
        MOTORL_IN1 = false;
        MOTORL_IN2 = false;
        TIM_SetCompare2(TIM4, 0);
    }

    if(pwmR >= 1000)
    {
        pwmR = 1000;
    }
    else if(pwmR <= -1000)
    {
        pwmR = -1000;
    }

    if(pwmR > 0)
    {
        MOTORR_IN1 = false;
        MOTORR_IN2 = true;
        TIM_SetCompare1(TIM4, pwmR);
    }
    else if(pwmR < 0)
    {
        MOTORR_IN1 = true;
        MOTORR_IN2 = false;
        TIM_SetCompare1(TIM4, -pwmR);
    }
    else
    {
        MOTORR_IN1 = false;
        MOTORR_IN2 = false;
        TIM_SetCompare1(TIM4, 0);
    }
}
