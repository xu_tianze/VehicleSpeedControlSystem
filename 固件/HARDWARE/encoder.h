/**
 * @file encoder.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 电机编码器驱动
 * @date 2021-04-22
 */

#ifndef __ENCODER_H__
#define __ENCODER_H__

#include <stdint.h>

void encoder_init(void);
int16_t encoder_get_speedL(void);
int16_t encoder_get_speedR(void);

#endif // __ENCODER_H__
