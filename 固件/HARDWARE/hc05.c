/**
 * @file hc05.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief hc05串口蓝牙模块驱动
 * @date 2021-04-22
 */

#include "hc05.h"
#include <stm32f10x.h>
#include <stdbool.h>

/**
 * @brief hc05模块初始化
 */
void hc05_init(void)
{
    GPIO_InitTypeDef  GPIO_Struct;

    RCC_APB2PeriphClockCmd(HC05_STATE_CLK | HC05_EN_CLK, ENABLE);

    GPIO_StructInit(&GPIO_Struct);
    GPIO_Struct.GPIO_Mode	= GPIO_Mode_IN_FLOATING;
    GPIO_Struct.GPIO_Pin	= HC05_STATE_PIN;
    GPIO_Init(HC05_STATE_GPIO, &GPIO_Struct);
    GPIO_Struct.GPIO_Mode	= GPIO_Mode_Out_PP;
    GPIO_Struct.GPIO_Pin	= HC05_EN_PIN;
    GPIO_Init(HC05_EN_GPIO, &GPIO_Struct);
    
    hc05_on();
}

/**
 * @brief 打开hc05模块
 */
void hc05_on(void)
{
    GPIO_SetBits(HC05_EN_GPIO, HC05_EN_PIN);
}

/**
 * @brief 关闭hc05模块
 */
void hc05_off(void)
{
    GPIO_ResetBits(HC05_EN_GPIO, HC05_EN_PIN);
}

/**
 * @brief 判断蓝牙是否已连接
 * @return true 已连接
 * @return false 未连接
 */
bool hc05_connected(void)
{
    return GPIO_ReadInputDataBit(HC05_STATE_GPIO, HC05_STATE_PIN);
}
