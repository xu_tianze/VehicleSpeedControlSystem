/**
 * @file hc05.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief hc05串口蓝牙模块驱动
 * @date 2021-04-22
 */

#ifndef __HC05_H__
#define __HC05_H__

#include <stdbool.h>

#define HC05_STATE_CLK      RCC_APB2Periph_GPIOA
#define HC05_STATE_PIN      GPIO_Pin_8
#define HC05_STATE_GPIO     GPIOA

#define HC05_EN_CLK         RCC_APB2Periph_GPIOA
#define HC05_EN_PIN         GPIO_Pin_11
#define HC05_EN_GPIO        GPIOA

void hc05_init(void);
void hc05_on(void);
void hc05_off(void);
bool hc05_connected(void);

#endif // __HC05_H__
