/**
 * @file timer.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 定时器配置
 * @date 2021-04-19
 */

#ifndef __TIMER_H__
#define __TIMER_H__

#include "sys.h"

void TIM1_interrupt_init(void(*handler)());

#endif // __TIMER_H__
