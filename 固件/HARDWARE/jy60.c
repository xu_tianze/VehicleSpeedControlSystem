/**
 * @file jy60.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief jy60串口陀螺仪模块驱动
 * @date 2021-04-22
 */

#include "jy60.h"
#include <stm32f10x.h>
#include <stdint.h>
#include <stdbool.h>

#define USARTx USART3

static const float g = 9.8; // 重力加速度
static float ax, ay, az, wx, wy, wz, roll, pitch, yaw, temperature;
static bool yaw_clear_flag = false;

enum{
    YAW_CLEAR   = 0x52, // 偏航角归零
    ACC_CORRECT = 0x67, // 加速度计校准
    SLEEP       = 0x60, // 休眠模式切换
    LEVEL       = 0x65, // 水平安装
    VERTICAL    = 0x66, // 垂直安装
};

/**
 * @brief 串口发送一个字节
 * @param data 需要发送的一字节数据
 */
static void usart_send_byte(uint8_t data)
{
    USARTx->DR = data;
    while(!(USARTx->SR & 0X40));
}

/**
 * @brief 发送指令
 * @param cmd 需要发送的指令
 */
static void send_cmd(uint8_t cmd)
{
    usart_send_byte(0xFF);
    usart_send_byte(0xAA);
    usart_send_byte(cmd);
}

/**
 * @brief 串口数据处理句柄，应放于串口中断服务函数中
 * @param res 串口读到的数据
 */
void jy60_handler(uint8_t res)
{
    static uint8_t receieveState = 0;
    static uint8_t pkgName = 0;
    static uint8_t checkSum = 0;
    static uint8_t data[4][2] = {0};

    switch(receieveState)
    {
        case 0: // 包头
            if(res == 0x55)
            {
                checkSum = res;
                receieveState++;
            }
            break;

        case 1: // 包名
            pkgName = res;
            checkSum += res;
            receieveState++;
            break;

        case 2: // 数据1低字节
            data[0][0] = res;
            checkSum += res;
            receieveState++;
            break;

        case 3: // 数据1高字节
            data[0][1] = res;
            checkSum += res;
            receieveState++;
            break;

        case 4: // 数据2低字节
            data[1][0] = res;
            checkSum += res;
            receieveState++;
            break;

        case 5: // 数据2高字节
            data[1][1] = res;
            checkSum += res;
            receieveState++;
            break;

        case 6: // 数据3低字节
            data[2][0] = res;
            checkSum += res;
            receieveState++;
            break;

        case 7: // 数据3高字节
            data[2][1] = res;
            checkSum += res;
            receieveState++;
            break;

        case 8: // 温度低字节
            data[3][0] = res;
            checkSum += res;
            receieveState++;
            break;

        case 9: // 温度高字节
            data[3][1] = res;
            checkSum += res;
            receieveState++;
            break;

        case 10: // 校验和
            if(res == checkSum)
            {
                switch(pkgName)
                {
                    case 0x51: // 加速度包
                        ax = (int16_t)(data[0][0]|(data[0][1] << 8)) / 32768.0 * 16 * g;
                        ay = (int16_t)(data[1][0]|(data[1][1] << 8)) / 32768.0 * 16 * g;
                        az = (int16_t)(data[2][0]|(data[2][1] << 8)) / 32768.0 * 16 * g;
                        temperature = (data[3][0]|(data[3][1] << 8)) / 340.0 + 36.53;
                        break;

                    case 0x52: // 角速度包
                        wx = (int16_t)(data[0][0]|(data[0][1] << 8)) / 32768.0 * 2000;
                        wy = (int16_t)(data[1][0]|(data[1][1] << 8)) / 32768.0 * 2000;
                        wz = (int16_t)(data[2][0]|(data[2][1] << 8)) / 32768.0 * 2000;
                        temperature = (data[3][0]|(data[3][1] << 8)) / 340.0 + 36.53;
                        break;

                    case 0x53: // 角度包
                        roll  = (int16_t)(data[0][0]|(data[0][1] << 8)) / 32768.0 * 180;
                        pitch = (int16_t)(data[1][0]|(data[1][1] << 8)) / 32768.0 * 180;
                        yaw   = (int16_t)(data[2][0]|(data[2][1] << 8)) / 32768.0 * 180;
                        temperature =    (data[3][0]|(data[3][1] << 8)) / 340.0 + 36.53;
                        break;
                }
            }
            receieveState = 0;
            break;
    }
}

/**
 * @brief 读取x轴加速度值
 * @return 最近一次读到的x轴加速度值
 */
float jy60_get_ax(void)
{
    return ax;
}

/**
 * @brief 读取y轴加速度值
 * @return 最近一次读到的x轴加速度值
 */
float jy60_get_ay(void)
{
    return ay;
}

/**
 * @brief 读取z轴加速度值
 * @return 最近一次读到的x轴加速度值
 */
float jy60_get_az(void)
{
    return az;
}

/**
 * @brief 读取x轴角速度值
 * @return 最近一次读到的x轴角速度值
 */
float jy60_get_wx(void)
{
    return wx;
}

/**
 * @brief 读取y轴角速度值
 * @return 最近一次读到的x轴角速度值
 */
float jy60_get_wy(void)
{
    return wy;
}

/**
 * @brief 读取z轴角速度值
 * @return 最近一次读到的x轴角速度值
 */
float jy60_get_wz(void)
{
    return wz;
}

/**
 * @brief 读取滚转角
 * @return 最近一次读到的滚转角
 */
float jy60_get_roll(void)
{
    return roll;
}

/**
 * @brief 读取俯仰角
 * @return 最近一次读到的俯仰角
 */
float jy60_get_pitch(void)
{
    return pitch;
}

/**
 * @brief 读取偏航角
 * @return 最近一次读到的偏航角
 */
float jy60_get_yaw(void)
{
    return yaw;
}

/**
 * @brief 读取扩充后的滚转角
 * @return 最近一次读到的扩充后的滚转角
 * @note 扩充是指将原本在0°~360°范围内的陀螺仪绝对式角度值扩充为+-360°的相对式角度值
 */
float jy60_get_roll_branched(void)
{
    static float lastRoll = 0;
    float nowRoll = jy60_get_roll();

    while(nowRoll - lastRoll + 10 >  360.0)
    {
        nowRoll -= 360.0;
    }
    while(nowRoll - lastRoll - 10 < -360.0)
    {
        nowRoll += 360.0;
    }

    lastRoll = nowRoll;
    return nowRoll;
}

/**
 * @brief 读取扩充后的俯仰角
 * @return 最近一次读到的扩充后的俯仰角
 * @note 扩充是指将原本在0°~360°范围内的陀螺仪绝对式角度值扩充为+-360°的相对式角度值
 */
float jy60_get_pitch_branched(void)
{
    static float lastPitch = 0;
    float nowPitch = jy60_get_pitch();

    while(nowPitch - lastPitch + 10 >  360.0)
    {
        nowPitch -= 360.0;
    }
    while(nowPitch - lastPitch - 10 < -360.0)
    {
        nowPitch += 360.0;
    }

    lastPitch = nowPitch;
    return nowPitch;
}

/**
 * @brief 读取扩充后的偏航角
 * @return 最近一次读到的扩充后的偏航角
 * @note 扩充是指将原本在0°~360°范围内的陀螺仪绝对式角度值扩充为+-360°的相对式角度值
 */
float jy60_get_yaw_branched(void)
{
    static float lastYaw = 0;
    float nowYaw = jy60_get_yaw();

    if(yaw_clear_flag)
    {
        lastYaw = 0;
        yaw_clear_flag = false;
        return nowYaw;
    }

    while(nowYaw - lastYaw + 10 >  360.0)
    {
        nowYaw -= 360.0;
    }
    while(nowYaw - lastYaw - 10 < -360.0)
    {
        nowYaw += 360.0;
    }

    lastYaw = nowYaw;
    return nowYaw;
}

/**
 * @brief 读取温度值
 * @return 最近一次读到的温度值
 */
float jy60_get_temperature(void)
{
    return temperature;
}

/**
 * @brief 偏航角归零
 */
void jy60_yaw_clear(void)
{
    yaw_clear_flag = true;
    send_cmd(YAW_CLEAR);
}

/**
 * @brief 校准加速度零偏
 */
void jy60_acc_correct(void)
{
    send_cmd(ACC_CORRECT);
}

/**
 * @brief 睡眠模式切换
 */
void jy60_sleep(void)
{
    send_cmd(SLEEP);
}

/**
 * @brief 设置安装方式为水平
 */
void jy60_set_level(void)
{
    send_cmd(LEVEL);
}

/**
 * @brief 设置安装方式为竖直
 */
void jy60_set_vertical(void)
{
    send_cmd(VERTICAL);
}
