/**
 * @file key.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 机械按键驱动
 * @date 2021-04-19
 */

#ifndef __KEY_H__
#define __KEY_H__

#include <stdint.h>
#include <stdbool.h>

#define KEY_LEFT_GPIO          GPIOB
#define KEY_LEFT_CLK           RCC_APB2Periph_GPIOB
#define KEY_LEFT_PIN           GPIO_Pin_12

#define KEY_LEFT_LED_CLK       RCC_APB2Periph_GPIOB
#define KEY_LEFT_LED_PIN       GPIO_Pin_13
#define KEY_LEFT_LED_GPIO      GPIOB

#define KEY_OK_CLK             RCC_APB2Periph_GPIOB
#define KEY_OK_PIN             GPIO_Pin_14
#define KEY_OK_GPIO            GPIOB

#define KEY_OK_LED_CLK         RCC_APB2Periph_GPIOB
#define KEY_OK_LED_PIN         GPIO_Pin_15
#define KEY_OK_LED_GPIO        GPIOB

#define KEY_RIGHT_CLK          RCC_APB2Periph_GPIOB
#define KEY_RIGHT_PIN          GPIO_Pin_5
#define KEY_RIGHT_GPIO         GPIOB

#define KEY_RIGHT_LED_CLK      RCC_APB2Periph_GPIOB
#define KEY_RIGHT_LED_PIN      GPIO_Pin_8
#define KEY_RIGHT_LED_GPIO     GPIOB

#define KEY_PRESS_LEVEL        false
#define KEY_LED_ON_LEVEL       true

#define KEY_LEFT_PRESS_SHORT   (uint8_t)(1)
#define KEY_LEFT_PRESS_LONG    (uint8_t)(2)
#define KEY_OK_PRESS_SHORT     (uint8_t)(3)
#define KEY_OK_PRESS_LONG      (uint8_t)(4)
#define KEY_RIGHT_PRESS_SHORT  (uint8_t)(5)
#define KEY_RIGHT_PRESS_LONG   (uint8_t)(6)

#define KEY_SCAN_FLITER_TIME   (uint16_t)(3)    // 按键消抖时间，0~50
#define KEY_SCAN_LONG_PRESS    (uint16_t)(400)  // 长按判断时间

void key_init(void);
void key_left_led_on(void);
void key_ok_led_on(void);
void key_right_led_on(void);
void key_all_led_on(void);
void key_left_led_off(void);
void key_ok_led_off(void);
void key_right_led_off(void);
void key_all_led_off(void);
void key_all_led_blink(void);
bool key_left_status(void);
bool key_ok_status(void);
bool key_right_status(void);
bool key_all_status(void);
uint8_t key_left_scan(void);
uint8_t key_ok_scan(void);
uint8_t key_right_scan(void);
uint8_t key_all_scan(void);

#endif // __KEY_H__
