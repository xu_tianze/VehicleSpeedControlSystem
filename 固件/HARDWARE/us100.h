/**
 * @file us100.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief US-100串口超声波模块驱动
 * @date 2021-04-22
 */

#ifndef __US100_H__
#define __US100_H__

#include <stdint.h>
#include <stdbool.h>

void us100_handler(uint8_t res[]);
void us100_measure_start(void);
float us100_measure(void);

#endif // __US100_H__
