/**
 * @file motor.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 电机驱动
 * @date 2021-04-19
 */

#ifndef __MOTOR_H__
#define __MOTOR_H__

#include "sys.h"
#include <stdint.h>

#define MOTOR_EN   PBout(1)
#define MOTORL_IN1 PAout(15)
#define MOTORL_IN2 PAout(12)
#define MOTORR_IN1 PBout(3)
#define MOTORR_IN2 PBout(4)

#define PWM_MAX 1000

void motor_init(void);
void motor_on(void);
void motor_off(void);
void motor_pwm_set(int pwmL, int pwmR);

#endif //__MOTOR_H__
