/**
 * @file pid.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief PID算法
 * @date 2021-04-19
 */

#ifndef __PID_H__
#define __PID_H__

typedef struct{
    float kp;                 // 比例系数
    float ki;                 // 积分系数
    float kd;                 // 微分系数
    float maxIntegralBias;    // 积分限幅
    float bias;               // 本次误差
    float lastBias;           // 上次误差
    float integralBias;       // 误差积分
    float integralBiasIgnore; // 积分忽略半径（当误差小于一定值时，不再计算积分）
}PID;

int PID_controler(PID *pid, float target, float current);

#endif // __PID_H__
