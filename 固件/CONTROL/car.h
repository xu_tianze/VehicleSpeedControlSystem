/**
 * @file car.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 控制算法
 * @date 2021-04-19
 */

#ifndef __CAR_H__
#define __CAR_H__

#include <stdbool.h>

void car_handler(void);
void car_setMaxVelocity(int max);
void car_setVelocityL(int target);
void car_setVelocityR(int target);
void car_setVelocity(int target);
void car_setCourse(float target);
void car_setDistance(float target);
int car_getVelocityL(void);
int car_getVelocityR(void);
float car_getDistance(void);
float car_getCourse(void);
void car_runEnable_reverse(void);
bool car_runEnable_status(void);
void car_useAngularPID_reverse(void);
bool car_useAngularPID_status(void);
void car_useDistancePID_reverse(void);
bool car_useDistancePID_status(void);

#endif // __CAR_H__
