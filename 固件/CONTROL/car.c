/**
 * @file car.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 控制算法
 * @date 2021-04-19
 */

#include "car.h"
#include <stdint.h>
#include <stdbool.h>
#include "pid.h"
#include "motor.h"
#include "encoder.h"
#include "ANO.h"
#include "us100.h"
#include "jy60.h"
#include "hc05.h"

PID VelocityLPID = { // 左电机速度PI
    .kp = 15,
    .ki = 1,
    .maxIntegralBias = 500,
};
PID VelocityRPID = { // 右电机速度PI
    .kp = 15,
    .ki = 1,
    .maxIntegralBias = 500,
};
PID AngularPID = {   // 角度PID
    .kp = 7.9,
    .ki = 0.07,
    .kd = 38,
    .maxIntegralBias = 500,
    .integralBiasIgnore = 0.5,
};
PID DistancePID = {  // 距离PID
    .kp = 250,
    .ki = 0,
    .kd = 477,
    .maxIntegralBias = 10,
    .integralBiasIgnore = 0.03,
};

static int maxVelocity = 100;
static int velocityTargetL = 0;
static int velocityTargetR = 0;
static int velocityTarget = 0;
static int courseTarget = 0;
static float distanceTarget = 0;
static int velocityL = 0;
static int velocityR = 0;
static float course = 0;
static float distance = 0;
static bool runEnable   = false;
static bool useAngularPID  = false;
static bool useDistancePID = false;

/**
 * @brief 变量限幅
 * @param variable 需要被限幅的变量
 * @param range 幅度的绝对值
 */
static void limiting(int* variable, int range)
{
    if(*variable >  range)
    {
        *variable =  range;
    }
    if(*variable < -range)
    {
        *variable = -range;
    }
}

/**
 * @brief 周期执行的控制流程
 */
void car_handler(void)
{
    int pwmL = 0, pwmR = 0;
    int vL = velocityTargetL, vR = velocityTargetR;
    static float lastDistance = 0;
    const float distanceFliterFactor = 0.4;

    velocityL = encoder_get_speedL();
    velocityR = encoder_get_speedR();
    distance  = us100_measure() * distanceFliterFactor + lastDistance * (1 - distanceFliterFactor);
    lastDistance = distance;
    course    = jy60_get_yaw_branched();

    if(runEnable == false)
    {
        motor_off();
        motor_pwm_set(0, 0);
    }
    else
    {
        motor_on();

        if(useDistancePID)
        {
            int pidOutput = PID_controler(&DistancePID, distanceTarget, distance) * 10;
            limiting(&pidOutput, velocityTarget < 20 ? 20 : velocityTarget);
            vL -= pidOutput;
            vR -= pidOutput;
        }
        if(useAngularPID)
        {
            int pidOutput = PID_controler(&AngularPID, courseTarget, course) / 10;
            vL += pidOutput;
            vR -= pidOutput;
        }

        limiting(&vL, maxVelocity);
        limiting(&vR, maxVelocity);
        pwmL += PID_controler(&VelocityLPID, (float)vL, velocityL);
        pwmR += PID_controler(&VelocityRPID, (float)vR, velocityR);

        limiting(&pwmL, PWM_MAX);
        limiting(&pwmR, PWM_MAX);

        motor_pwm_set(pwmL, pwmR);
    }
    if(hc05_connected())
    {
        ANO_send_PID_debug_data(0xF1, vL, velocityL, pwmL / 10);
        ANO_send_PID_debug_data(0xF2, vR, velocityR, pwmR / 10);
    }
}

/**
 * @brief 设置车辆速度上限
 * @param max 车辆速度上限
 */
void car_setMaxVelocity(int max)
{
    maxVelocity = max;
}

/**
 * @brief 设置左电机目标速度
 * @param target 目标速度
 */
void car_setVelocityL(int target)
{
    velocityTargetL = target;
}

/**
 * @brief 设置右电机目标速度
 * @param target 目标速度
 */
void car_setVelocityR(int target)
{
    velocityTargetR = target;
}

/**
 * @brief 设置车辆目标速度
 * @param target 目标速度
 */
void car_setVelocity(int target)
{
    velocityTarget = target;
    // car_setVelocityL(target);
    // car_setVelocityR(target);
}

/**
 * @brief 设置车辆目标航向
 * @param target 目标航向
 */
void car_setCourse(float target)
{
    courseTarget = target;
}

/**
 * @brief 设置车辆与前方障碍物目标距离
 * @param target 目标距离
 */
void car_setDistance(float target)
{
    distanceTarget = target;
}

/**
 * @brief 获取左电机转速
 * @return 左电机转速
 */
int car_getVelocityL(void)
{
    return velocityL;
}

/**
 * @brief 获取右电机转速
 * @return 右电机转速
 */
int car_getVelocityR(void)
{
    return velocityR;
}

/**
 * @brief 获取障碍物距离
 * @return 障碍物距离
 */
float car_getDistance(void)
{
    return distance;
}

/**
 * @brief 获取车辆航向
 * @return 车辆航向
 */
float car_getCourse(void)
{
    return course;
}

/**
 * @brief 翻转运动使能标志位
 */
void car_runEnable_reverse(void)
{
    runEnable = !runEnable;
}

/**
 * @brief 获取运动使能标志位当前状态
 * @return true 
 * @return false 
 */
bool car_runEnable_status(void)
{
    return runEnable;
}

/**
 * @brief 翻转角度PID使能标志位
 */
void car_useAngularPID_reverse(void)
{
    useAngularPID = !useAngularPID;
}

/**
 * @brief 获取角度PID标志位当前状态
 * @return true 
 * @return false 
 */
bool car_useAngularPID_status(void)
{
    return useAngularPID;
}

/**
 * @brief 翻转距离PID使能标志位
 */
void car_useDistancePID_reverse(void)
{
    useDistancePID = !useDistancePID;
}

/**
 * @brief 获取距离PID标志位当前状态
 * @return true 
 * @return false 
 */
bool car_useDistancePID_status(void)
{
    return useDistancePID;
}
