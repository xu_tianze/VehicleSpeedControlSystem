/**
 * @file pid.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief PID算法
 * @date 2021-04-19
 */

#include "pid.h"

#define abs(x) (x >= 0 ? x : -x)

/**
 * @brief 绝对式PID控制器
 * @param pid PID结构体
 * @param target 目标值
 * @param current 当前值
 * @return 控制输出量
 */
int PID_controler(PID *pid, float target, float current)
{
    int output = 0;

    pid->bias = target - current;                                   // 计算本次误差
    if(pid->ki != 0 || abs(pid->bias) > pid->integralBiasIgnore)    // 当积分系数为0时或误差小于误差忽略半径，不对积分进行处理，退化为PD控制器
    {
        pid->integralBias += pid->bias;
        if(pid->maxIntegralBias > 0)                                // 当积分限幅值不为0时，对积分进行限幅
        {
            if(pid->integralBias > pid->maxIntegralBias)
            {
                pid->integralBias = pid->maxIntegralBias;
            }
            if(pid->integralBias < -pid->maxIntegralBias)
            {
                pid->integralBias = -pid->maxIntegralBias;
            }
        }
    }
    else
    {
        pid->integralBias = 0;
    }

    output = pid->kp * pid->bias                                    // 使用离散PID公式计算输出量
           + pid->ki * pid->integralBias
           + pid->kd * (pid->bias - pid->lastBias);
    pid->lastBias = pid->bias;                                      // 保存本次误差作为下一轮PID计算中的上次误差

    return output;
}
