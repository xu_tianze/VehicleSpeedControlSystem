/**
 * @file ANO.c
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 匿名上位机V7通信协议对接
 * @date 2021-04-19
 */

#include "ANO.h"
#include <stm32f10x.h>
#include <stdint.h>
#include <stdbool.h>

#define USARTx USART1

static const uint8_t HEAD = 0xAA;
static const uint8_t D_ADDR = 0xFF;

#define USART_SEND_BUFF_LEN 200
static uint8_t usartSendBuff[USART_SEND_BUFF_LEN];
static uint16_t buffHead = 1, buffTail = 1;

/**
 * @brief 缓冲区判空
 * @return true 
 * @return false 
 */
static bool sendBuff_empty(void)
{
    if(buffTail == buffHead)
    {
        return true;
    }
    return false;
}

/**
 * @brief 向缓冲区填入数据
 * @param data 需要填入的数据
 */
static void sendBuff_put(uint8_t data)
{
    usartSendBuff[buffHead++ - 1] = data;
    if(buffHead > USART_SEND_BUFF_LEN)
    {
        buffHead = 1;
    }
}

/**
 * @brief 从缓冲区取出数据
 * @return 取出的数据
 */
static uint8_t sendBuff_get(void)
{
    uint8_t ret = 0;
    if(!sendBuff_empty())
    {
        ret =  usartSendBuff[buffTail++ - 1];
    }
    if(buffTail > USART_SEND_BUFF_LEN)
    {
        buffTail = 1;
    }
    return ret;
}

/**
 * @brief 串口发送一个字节
 * @param data 需要发送的一字节数据
 */
static void usart_send_byte(uint8_t data)
{
    USARTx->DR = data;
    while(!(USARTx->SR & 0X40));
}

/**
 * @brief 发送数据帧
 * @param ID 功能码
 * @param LEN 数据长度
 * @param DATA 数据内容（数组，长度为LEN）
 */
void ANO_send_frame(uint8_t ID, uint8_t LEN, uint8_t DATA[])
{
    uint8_t i,SUM_CHECK = 0,ADD_CHECK = 0;
    //计算SUM_CHECK和ADD_CHECK时，
    //因为uint8_t只能存八位二进制且溢出后自动清零，所以不需要显式地通过取余求低八位。
    sendBuff_put(HEAD);    SUM_CHECK += HEAD;      ADD_CHECK += SUM_CHECK;
    sendBuff_put(D_ADDR);  SUM_CHECK += D_ADDR;    ADD_CHECK += SUM_CHECK;
    sendBuff_put(ID);      SUM_CHECK += ID;        ADD_CHECK += SUM_CHECK;
    sendBuff_put(LEN);     SUM_CHECK += LEN;       ADD_CHECK += SUM_CHECK;
    for(i = 0; i < LEN; i++)
    {
        sendBuff_put(DATA[i]);
        SUM_CHECK += DATA[i];
        ADD_CHECK += SUM_CHECK;
    }
    sendBuff_put(SUM_CHECK);
    sendBuff_put(ADD_CHECK);
}

/**
 * @brief 发送欧拉角
 * @param roll 横滚角
 * @param pitch 俯仰角
 * @param yaw 偏航角
 */
void ANO_send_euler_angle(float roll, float pitch, float yaw)
{
    uint8_t data[7];

    data[0] = (int16_t)(roll  * 100) % 256;
    data[1] = (int16_t)(roll  * 100) / 256;
    data[2] = (int16_t)(pitch * 100) % 256;
    data[3] = (int16_t)(pitch * 100) / 256;
    data[4] = (int16_t)(yaw   * 100) % 256;
    data[5] = (int16_t)(yaw   * 100) / 256;
    data[6] = (int16_t)0;

    ANO_send_frame(0X03, sizeof(data), data);
}

/**
 * @brief 发送PID调试用的数据
 * @param ID 数据包ID（0XF1~0XFA）
 * @param target 目标值
 * @param current 当前值
 * @param output 输出值
 */
void ANO_send_PID_debug_data(uint8_t ID, int16_t target, int16_t current, int16_t output)
{
    uint8_t data[6];

    data[0] = (int16_t)(target  * 100) % 256;
    data[1] = (int16_t)(target  * 100) / 256;
    data[2] = (int16_t)(current * 100) % 256;
    data[3] = (int16_t)(current * 100) / 256;
    data[4] = (int16_t)(output  * 100) % 256;
    data[5] = (int16_t)(output  * 100) / 256;

    ANO_send_frame(ID, sizeof(data), data);
}

/**
 * @brief 放于主循环中，发送循环队列中的待发送数据
 */
void ANO_main(void)
{
    uint8_t breakCnt = 0;
    while(!sendBuff_empty())
    {
        usart_send_byte(sendBuff_get());
        if(breakCnt++ >= 20)
        {
            break;
        }
    }
}
