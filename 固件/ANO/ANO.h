/**
 * @file ANO.h
 * @author BadFatCat0919 (543015378@qq.com)
 * @brief 匿名上位机V7通信协议对接
 * @date 2021-04-19
 */

#ifndef __ANO_H__
#define __ANO_H__

#include <stm32f10x.h>
#include <stdint.h>

void ANO_send_frame(uint8_t ID, uint8_t LEN, uint8_t DATA[]);
void ANO_send_euler_angle(float roll, float pitch, float yaw);
void ANO_send_PID_debug_data(uint8_t ID, int16_t target, int16_t current, int16_t output);
void ANO_main(void);

#endif //__ANO_H__
